<?php

namespace App\Presenters;
use App\Model\MenuModel;
use App\Component\Menu\Menu;

abstract class BasePresenter extends \Nette\Application\UI\Presenter
{
    private MenuModel $menuModel;
    
    public function injectDepencies(MenuModel $menuModel) 
    {
        $this->menuModel = $menuModel;
    }
    
    protected function beforeRender()
    {
        parent::beforeRender();
        
        $this->template->header = $this->menuModel->getHederData();

        $this->template->footer = $this->menuModel->getFooterData();
    }

    protected function createComponentMenu() {
        return new Menu($this->menuModel);
    }
}