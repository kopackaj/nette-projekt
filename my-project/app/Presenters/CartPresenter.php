<?php

declare(strict_types=1);

namespace App\Presenters;

use Nette;
use Nette\Application\UI\Form;
use App\Model\OrdersModel;


final class CartPresenter extends BasePresenter
{
    private OrdersModel $ordersModel;

    public function __construct(OrdersModel $ordersModel){
        $this->ordersModel = $ordersModel;
    }
    
	public function actionShipping(){
    }

    public function actionOrder(){
    }

    protected function createComponentOrderForm(): Form
    {
        $form = new Form;
        $form->addText('name')->setRequired();
        $form->addText('email')->setRequired();
        $form->addText('adress')->setRequired();
        $form->addText('tel')->setRequired();
        $form->addText('comment');
        $form->addSubmit('send');
        $form->onSuccess[] = [$this, 'orderSucceded'];
        return $form;
    }

    public function orderSucceded(Form $form, $data) : void {
        $this->ordersModel->InsertOrder($data->name, $data->email, $data->adress, $data->tel, $data->comment);
        //$this->flashMessage('Jo povedlo se');
    }
}