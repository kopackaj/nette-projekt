<?php
 
declare(strict_types=1);
 
namespace App\Model;
 
use Nette;
 
final class OrdersModel
{
    public function __construct(
        private Nette\Database\Connection $database,
        private Nette\Database\Explorer $explorer,
    ){
    }

    public function InsertOrder($name, $email, $adress, $tel, $comment){
        $this->explorer->table("customers")->insert(["email" => $email, "name" => $name, "adress" => $adress, "tel" => $tel, "comment" => $comment]);
    }
}