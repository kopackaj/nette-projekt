<?php

declare(strict_types=1);

namespace App\Model;

use Nette;

final class ProductModel
{
	public function __construct(
		private Nette\Database\Connection $database,
        private Nette\Database\Explorer $explorer,
	) {
	}
    
    public function getData() {
        $result = $this->database->query('SELECT * FROM products');

        return $result;
    }

    public function getDataById($id){
        $vysledek = null;
        $vsechnyProdukty = $this->database->query('SELECT * FROM products WHERE id = ?',$id);
        $vysledek = $vsechnyProdukty->fetch();

        return $vysledek;
    }

    public function getProductsByCategory($category=null) {
        $id_category = $this->explorer->table('categories')->select("id")->where("name",$category);
        
        return $this->explorer->table("products")->select("*")->where("category",$id_category);
    }

    public function getDataByPage($page, $itemsPerPage) {
        return $this->explorer->table("products")->select("*")->page((int)$page, (int)$itemsPerPage);
    }
    
    public function getDataByPageAndCategory($page, $itemsPerPage, $category=null) {
        $id_category = $this->explorer->table('categories')->select("id")->where("name",$category);

        bdump($page." ".$itemsPerPage);

        return $this->explorer->table("products")->select("*")->where("category",$id_category)->page((int)$page, (int)$itemsPerPage);
    }
}