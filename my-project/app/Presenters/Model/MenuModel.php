<?php

declare(strict_types=1);

namespace App\Model;

use Nette;

final class MenuModel
{
    public function getHederData() : array {
        return [
            "tabulkaVelikosti" => "Tabulka velikostí",
            "faqs" => "FAQs",
            "kontakty" => "Kontakty",
        ];
    }

    public function getFooterData() : array {
        return [
            [
                "sectionTitle" => "Section",
                "sub" => [
                    "Home","Features","Prices","FAQs","About",
                ] 
            ],
            [
                "sectionTitle" => "Section",
                "sub" => [
                    "Home","Features","Prices","FAQs","About",
                ] 
            ],
            [
                "sectionTitle" => "Section",
                "sub" => [
                    "Home","Features","Prices","FAQs","About",
                ] 
            ],
        ];
    }
}