<?php

declare(strict_types=1);

namespace App\Presenters;

use Nette;
use App\Model\ProductModel;
use Tracy\Debugger;

final class ProductsPresenter extends BasePresenter
{
	private ProductModel $productModel;
    
    public function __construct(
        ProductModel $productModel,
    ) {
        $this->productModel = $productModel;
    }
    
    public function actionDefault($category=null) : void 
    {
        $httpRequest = $this->getHttpRequest(); //proc je to tu??? a ne v konstruktoru
        $page = $httpRequest->getQuery('page');

        if (!$category && !$page) {
            $this->template->productList =
                $this->productModel->getData();

        } elseif ($category && !$page) {
            $this->template->productList =
                $this->productModel->getProductsByCategory($category);

        } elseif (!$category && $page){
            $itemsPerPage = 6;
            $page = $httpRequest->getQuery('page');
            $this->template->productList = 
                $this->productModel->getDataByPage($page, $itemsPerPage);

        } else {
            $itemsPerPage = 6;
            $page = $httpRequest->getQuery('page');
            $this->template->productList = 
                $this->productModel->getDataByPageAndCategory($page, $itemsPerPage, $category);
        }
    }

    public function actionDetail($id)
    {
        $this->template->products =
            $this->productModel->getDataById($id);
    }
}