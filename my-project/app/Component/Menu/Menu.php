<?php

declare(strict_types=1);

namespace App\Component\Menu;

use Nette;
use Nette\Application\UI\Presenter;
use App\Model\MenuModel;
use Nette\Application\UI\Control;

final class Menu extends Control
{
    public function __construct(
        private MenuModel $menuModel,
    )
    {
        
    }

    public function render(){
        $this->template->links = $this->menuModel->getHederData();

        $this->template->render(__DIR__ . '/menu.latte');
    }
}